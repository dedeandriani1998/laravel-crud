<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){

        $product = Product::All();
        return view('product/index',compact('product'));
    }

    public function create(){
        $getRow = Product::orderBy('id', 'DESC')->get();
        $rowCount = $getRow->count();
        
        $lastId = $getRow->first();

        $kode = "A1000";
        
        if ($rowCount > 0) {
            if ($lastId->id < 9) {
                    $kode = "A100".''.($lastId->id + 1);
            } else if ($lastId->id < 99) {
                    $kode = "A10".''.($lastId->id + 1);
            } else if ($lastId->id < 999) {
                    $kode = "A1".''.($lastId->id + 1);
            } 
        }
       return view('product/add',compact('kode'));
    }

    // public function json(){
    //     return Datatables::of(Obat::all())->make(true);
    // }

    public function store(Request $request){

        $request->validate([
            'name'        => 'required',
            'description' => 'required',
        ]);

        $getRow = Product::orderBy('id', 'DESC')->get();
        $rowCount = $getRow->count();
        
        $lastId = $getRow->first();

        $kode = "A1000";
        
        if ($rowCount > 0) {
            if ($lastId->id < 9) {
                    $kode = "A100".''.($lastId->id + 1);
            } else if ($lastId->id < 99) {
                    $kode = "A10".''.($lastId->id + 1);
            } else if ($lastId->id < 999) {
                    $kode = "A1".''.($lastId->id + 1);
            } 
        }

  	    $product = new Product();
  	    $product->name        = $kode;
		$product->description = $request->input('description');
		$product->price       = $request->input('price');
		$product->qty         = $request->input('qty');
		$product->save(); 
		return redirect('/product')->with('success','Data berhasil disimpan');
    }

    public function update(Request $request, $id){
        $this->validate($request, [
          'name' => 'required',
          'description' => 'required',
          'price' => 'required',
          'qty' => 'required'
        ]);

        $product                 = Product::where('id', $id)->first();
        $product->name           = $request['name'];
        $product->description    = $request['description'];
        $product->price          = $request['price'];
        $product->qty            = $request['qty'];
        $product->update();

		return redirect('/product')->with('success','Data berhasil diupdate');
    }

    public function edit($id){
        $product = Product::where('id', $id)->get();
		return view('product/edit',compact('product'));
    }

    public function destroy($id){
        $obat = Product::where('id',$id)->delete();
		return redirect('/product')->with('success','Data berhasil dihapus');
    }
}
