<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $product  = Product::count();
    //     $jumlah_matakuliah = Matakuliah::count();
    //     $jumlah_jurusan    = Jurusan::count();
    //     $jumlah_fakultas   = Fakultas::count();
        return view('dashboard',compact('product'));
    }
}
