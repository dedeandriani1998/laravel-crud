<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table    = 'tb_product';
    protected $fillable = ['id','name','description','price','qty'];
    public    $timestamps      = false;
}
